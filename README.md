# New logo for LinuxOptimus

I've made a logo upgrade for my LinuxOptimus (a.k.a. "Как приручить трансформера") projects (YouTube, VK.com, Instagram, Telegram etc.)

And after making a new fancy vector logo, I've also wrapped it into a React component to be able to use it in my web projects as a logo, as an icon, or as a media for demoing my JS/CSS experiments.

---

## Public site URL (served with GitLab pages)

Here is a story behind this logo upgrade.

English version: [https://webon.gitlab.io/optimus-logo/#/en](https://webon.gitlab.io/optimus-logo/#/en)

Russian version: [https://webon.gitlab.io/optimus-logo/#/ru](https://webon.gitlab.io/optimus-logo/#/ru)

---

## New logo React component

You can find this component at `src/components/logo/OptimusLogo.jsx`

OptimusLogo component usage examples:

```
  <OptimusLogo width="10rem" />
  <OptimusLogo blind stroke="black" fill="red" strokeWidth="4" />
  <OptimusLogo eyesStroke="#000000" eyesFill="#ff0000" eyesStrokeWidth="4" />
```

---

![LinuxOptimus Logo](https://webon.gitlab.io/optimus-logo/apple-touch-icon.png)

[![CC License BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/) LinuxOptimus Logo by Oleg Kiselevich

is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)
