import OptimusLogo from '../components/logo/OptimusLogo';

export const pageText = {
  "title" : {
    "en" : 
      <>
        How I've made a logo upgrade for <br /> 
        LinuxOptimus <br /> 
        <small>(a.k.a. "Как приручить трансформера")</small>
      </>,
    "ru" : 
      <>
        Как я обновил логотип канала <br /> 
        "Как приручить трансформера" <br /> 
        <small>(другое название LinuxOptimus)</small>
      </>
  },

  "text1" : {
    "en" :
      <>
        <p>The story began when I took my YouTube channel's logo and tried to play with it in <em>kdenlive</em> video editor...</p>
        
        <p>This is how the logo used to look (which is not too bad by the way).</p>
      </>
    ,
    "ru" : 
      <>
        <p>Всё началось, когда я взял логотип моего YouTube канала и попробовал поиграться с ним в видео редакторе <em>kdenlive</em>...</p>
        
        <p>Вот каким логотип был раньше (кстати он выглядел вполне неплохо).</p>
      </>
  },

  "text2" : {
    "en" :
      <p>You see, when you try to make a fancy <em>matte</em> video transition effect, the image for it should be black and white or black/white and transparent. Which means having a monochrome logo or at least a monochrome version of it would be very handy. Besides, the older logo was not created by myself from scratch, but was simply downloaded from some open source resource and modified a bit. So it would be good plan to have a properly authored logo. Those were my thoughts that made me play with <em>Inkscape</em> for couple of evenings. </p>,
    "ru" : 
      <p>Дело в том, что для создания эффектного видео перехода типа <em>matte</em> необходимо использовать изображение либо черно-белого цвета, либо с сочетанием прозрачного и черного или белого. А значит было бы удобно иметь под рукой монохромный логотип, или хотя бы монохромную его версию. Кроме того, существующий лого не был создан мной лично, а я просто скачал его из какого-то открытого источника и немного подправил. Так что было бы целесообразно в итоге иметь полностью авторский логотип. Таковы были мои размышления, из-за которых я пару вечеров развлекался рисованием в <em>Inkscape</em>. </p>
  },

  "text3" : {
    "en" :
      <p>As a result of many stroke drawings as well as "control-Zets" and "control-shift-Zets" I came up with a vector Optimus logo. You know, if you want to follow the mainstream design tendencies, start simplifying everything, convert logos to symbols, photos to illustrations and complex images to plain icons. </p>,
    "ru" : 
      <p>В результате многочисленных зарисовок, а также множества "контрол-зет" и "контрол-шифт-зет", у меня получился новый векторный логотип Оптимуса. Так получается, что в погоне за современными тенденциями в дизайне начинаешь всё упрощать, превращая логотипы в символы, фотографии в иллюстрации, а сложные изображения в простые значки. </p>
  },

  "text4" : {
    "en" :
      <p>Yes, I really like how it looks now. But let’s add couple of variations. In case I want to use this logo in a video intro transition, it should rather be filled with white color on black or transparent background. </p>,
    "ru" : 
      <p>О да, мне и в самом деле нравится, как логотип выглядит теперь. Но давайте добавим еще несколько вариаций. К тому же, для использования его в эффекте видео перехода следует залить лого белым цветом и поместить на черном или прозрачном фоне. </p>
  },

  "text5" : {
    "en" :
      <p>Let’s try how it could look when used in a transition.</p>,
    "ru" : 
      <p>И давайте попробуем, как может выглядеть подобный эффект перехода. </p>
  },

  "showYouTubeLink" : {
    "en" :
      <>Can't see the video above? Click here</>
    ,
    "ru" :
      <>Не отображается видео? Кликай сюда</>
  },

  "showVideoLink" : {
    "en" :
      <>Click to get back to original video</>
    ,
    "ru" :
      <>Кликай, чтобы вернуться к изначальному видео</>
  },

  "text6" : {
    "en" :
      <>
        <p>Nnnn-oice!</p>

        <p>Now, what if I want to reuse the logo for other cases, like imprinting it on a T-shirt or a notebook…  Or maybe even printing it as a sticker. Or even creating a glowing logo for my laptop. Wow, what an idea!</p>

        <p>Since it’s a vector image, it is pretty easy to play with it and produce different variations.</p>
      </>
    ,
    "ru" : 
      <>
        <p>Кру-у-у-уть!</p>

        <p>А что если я захочу использовать логотип и для других целей, например напечатать его на футболке или на обложке блокнота... Или использовать его для печати наклеек. Или даже создать светящийся логотип для моего ноутбука. Ого, крутая идея, кстати!</p>

        <p>Поскольку это векторное изображение, то модифицировать его для создания различных вариаций достаточно просто.</p>
      </>
  },

  "text7" : {
    "en" :
      <>
        <p>Cool!</p>

        <p>Wait a sec. I’ve just remembered that I’m a web developer. So, how about creating a reusable SVG that could be used as a web page logo or an icon or a favicon or what not? </p>

        <p>Let’s dig into the code, generated by Inkscape. By the way, you can do it with any SVG image file. In essence an .svg file is just an XML document. </p>
      </>
    ,
    "ru" : 
      <>
        <p>Отлично!</p>

        <p>Погодите-ка. Я вот вспомнил, что занимаюсь веб разработкой. А как насчет того, чтобы создать SVG элемент, который можно будет использовать в качестве логотипа на веб странице или иконки или фавикона да и кто знает в качестве чего ещё?</p>

        <p>Давайте заглянем в код, сгенерированный Inkscape. Кстати, подобный фокус можно проделать с любым SVG файлом. По сути .svg файл - это просто XML документ. </p>
      </>
  },

  "text8" : {
    "en" :
      <>
        <p>Ok, it has a lot of service info that we can get rid of, because we don’t need it for the web. (I'm going to use a regular expression <code>\s[\w]+:[\w\-]+="[\w.,:;\-\/#\s()]+"</code> to quickly find all namespaced attributes.)</p>

        <p>Now, if I want to reuse it in many places and even across projects I could wrap it as a separate component. React component. Why not?</p>
      </>
    ,
    "ru" : 
      <>
        <p>Итак, мы видим много сервисных данных, которые можем спокойно удалить, поскольку в них нет необходимости для веб применения. (Я использую регулярное выражение <code>\s[\w]+:[\w\-]+="[\w.,:;\-\/#\s()]+"</code> для быстрого поиска именных атрибутов.)</p>

        <p>Ну а для того, чтобы использовать веб логотип где угодно и даже в разных проектах, почему бы не создать на его основе отдельный компонент. React компонент.</p>
      </>
  },

  "text9" : {
    "en" :
      <p>And now let’s add some options. I would like to be able to create logo variations with settings rather than creating a separate component each time. </p>
    ,
    "ru" : 
      <p>Теперь добавим немного настроек. Ведь я бы хотел создавать вариации логотипа путем изменения значений в настройках, а не тупо создавая новый компонент каждый раз. </p>
  },

  "text10" : {
    "en" :
      <p>Look at that! Looks pretty neat, doesn’t it? </p>
    ,
    "ru" : 
      <p>Только полюбуйтесь! Выглядит вполне чётко, не находите?</p>
  },

  "text11" : {
    "en" :
      <p>By the way, I’ve just remembered that SVG allows for almost any type of transforms and transitions, which means we could animate this logo. And since it consists of several completely separated parts, we could animate each part individually. Shall we give it a try?</p>
    ,
    "ru" : 
      <p>Кстати, я вот ещё вспомнил, что SVG открывает возможность практически к любым трансформациям и переходам, а значит можно добавить логотипу анимации. А раз рисунок представляет собой несколько частей, не соединенных друг с другом, значит можно отдельно анимировать каждую из них. Ну что, попробуем?</p>
  },

  "text12" : {
    "en" :
      <>
        <p>
          Awesome! As you can see, it can be really functional. When used for the web, CSS and JavaScript could give us endless number of ways to play with it. 
        </p>

        <p>
          For me it's not just a logo upgrade, but something more. If you check out my YouTube channel, you could spot that I haven't been very active there lately. I  experimented with different video formats and tried to improve the quality of my content. I even tried to blog on VK and Telegram every day for couple weeks. But somehow I kept loosing momentum.</p>
          
        <p>
          Now, this logo upgrade could seem like another attempt to get back on track. But I see it as a declaration to myself and my followers. This logo is not just a picture, but rather a symbol I would like to associate myself with. As a blogger, as a creator, as a developer. Each time I look at it, I feel like creating. Optimus'es eyes are glowing bright
          
          <OptimusLogo 
            width="1.5em"
            fill="teal" 
            stroke="white" 
            strokeWidth="2"
            eyesFill="red" 
            eyesStroke="white"
            eyesStrokeWidth="0"
            style={{margin: '-.25em .25em'}}
            className="glowing-eyes"
          />
          
          to see lots of my ideas released. So, wish me luck. And follow my channels to see how it goes.
        </p>
      </>
    ,
    "ru" : 
      <>
        <p>
          Обалденно! Как видите, этот компонент может быть достаточно функциональным. При использовании в веб разработке, возможности того, как с ним можно играться, просто безграничны благодаря CSS и JavaScript. 
        </p>

        <p>
          Лично для меня это не просто обновление логотипа, а нечто большее. Если вы следили за моим YouTube каналом, то могли заметить, что в последнее время я не был там очень уж активным. Я пробовал менять формат подачи материала в своих видео, старался повысить качество контента. Я даже пробовал публиковать ежедневные посты в VK и Telegram в течение нескольких недель. Но каждый раз всё равно почему-то постепенно терял импульс.</p>
          
        <p>
          И возможно кому-то это обновление логотипа может показаться просто очередной попыткой снова войти в колею. Но для меня это своего рода заявка себе самому и подписчикам. Это не просто новая картина, а скорее символ, с которым мне хочется, чтобы ассоциировался я как блогер, как создатель контента, как разработчик. Каждый раз, когда я смотрю на этот логотип, внутри появляется желание творить. Глаза Оптимуса продолжают ярко гореть
          
          <OptimusLogo 
            width="1.5em"
            fill="teal" 
            stroke="white" 
            strokeWidth="2"
            eyesFill="red" 
            eyesStroke="white"
            eyesStrokeWidth="0"
            style={{margin: '-.25em .25em'}}
            className="glowing-eyes"
          />
          
          , чтобы увидеть, как будут реализованы еще множество моих идей. Так что пожелайте мне удачи. И подписывайтесь на каналы, чтобы следить за тем, что из этого выйдет.
        </p>
      </>
  },

  "text13" : {
    "en" :
      <p>Bye. See you around... </p>
    ,
    "ru" : 
      <p>Пока. Еще увидимся... </p>
  },

  "downloadLink" : {
    "en" :
      <>Download LinuxOptimus SVG logo</>
    ,
    "ru" : 
      <>Скачать SVG логотип LinuxOptimus</>
  },

};