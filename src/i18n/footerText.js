export const footerText = {
  "logo_title" : {
    "en" : 
      <>LinuxOptimus Logo</>
    ,
    "ru" :
      <>Логотип LinuxOptimus,</>
  },
  "by" : {
    "en" : 
      <>by</>
    ,
    "ru" :
      <>создан</>
  },
  "author" : {
    "en" : 
      <>Oleg Kiselevich</>
    ,
    "ru" :
      <>Олегом Киселевичем,</>
  },
  "text1" : {
    "en" : 
      <>is licensed under a</>
    ,
    "ru" :
      <>защищён лицензией</>
  },
  "text2" : {
    "en" : 
      <>This webpage has an open source code which is available at</>
    ,
    "ru" :
      <>Данная веб страница имеет открытый исходный код, который доступен в</>
  },
  "text3" : {
    "en" : 
      <>Date of first publication</>
    ,
    "ru" :
      <>Дата первой публикации</>
  },
  "date" : {
    "en" : 
      <>04 November 2020</>
    ,
    "ru" :
      <>04 ноября 2020</>
  },
};
