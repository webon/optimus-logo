import React from 'react'
import LazyLoad from 'react-lazy-load';
import { useState } from 'react';

import './ImageLoader.css';

export default function ImageLoader(props) {
  const [loaded, setLoaded] = useState(false);
  const theHeight = props.aspect ? (100 / props.aspect) + '%' : '100%';

  return (
    <LazyLoad debounce={false} offsetVertical={500}>
      <div className={`image-container ${loaded ? '' : 'loading'}`}
        style={{
          width: (props.width || '20rem'),
          paddingBottom: theHeight
        }}
      >
        <img 
          src={props.src} 
          alt={props.alt}
          onLoad={() => {setLoaded(true)}} 
          className={`image-${loaded ? 'loaded' : 'loading'}`}
        />
      </div>
    </LazyLoad>
  );
}
