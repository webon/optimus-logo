import React from 'react';

import './App.css';
import OptimusLogo from './logo/OptimusLogo';

import ImageLoader from './ImageLoader';
import YouTubeEmbed from './YouTubeEmbed';

import { useRef, useState, useEffect } from "react";

import oldLogo from '../assets/img/optimus-old-transparent.png';
import inkscapePhoto from '../assets/img/inkscape-process.png';
import svgCodePhoto from '../assets/img/svg-inkscape-code.png';

import logoReactComponentCode from '../assets/img/logo-react-component.png';
import logoReactOptionsCode from '../assets/img/logo-react-options.png';

import LogoVariations from './LogoVariations';
import PlayLogoOptions from './PlayLogoOptions';
import PlayLogoAnimation from './PlayLogoAnimation';
import ChannelsLinks from './ChannelsLinks';
import Footer from './Footer';

import logoTransitionPoster from '../assets/video/logo-transition-poster.png';

import { pageText } from '../i18n/pageText';
import LangSelect from './LangSelect';

function App() {
  const mainRef = useRef(null);
  const scrollToMain = () => {
    mainRef.current.scrollIntoView({ behavior: 'smooth' });
    window.gtag('event', 'click_scrollDown_arrow');
  };

  const [videoFailed, setVideoFailed] = useState(false);

  const videoFallback = (e) => {
    e.preventDefault();
    setVideoFailed(!videoFailed);
    window.gtag('event', 'video_fallback', { 'asked_for' : (videoFailed ? 'YouTube' : 'Local HTML5') });
  };

  const [lang, setLang] = useState('ru');
  const didMount = true;

  useEffect(() => {
    if (window.location.hash === '#/en') {
      document.querySelector('html').lang = 'en';
      setLang('en');
    }
  }, [didMount]);

  const selectLang = (e) => {
    const newLang = e.target.rel;
    const where = e.target.dataset?.where || 'undetected';

    window.gtag('event', 'select_language', {'selected_language' : newLang, 'selector' : where});
    document.querySelector('html').lang = newLang;
    setLang(newLang);
  }

  return (
    <>
      <header>
        <LangSelect lang={lang} onSelectLang={selectLang} where="header" />

        <div className="main-content">
          <h1>
            {pageText.title[lang]}
          </h1>
        </div>

        <div className="media-container">
          <div className="header--old-logo">
            <ImageLoader src={oldLogo} alt="Old logo" width="10em" />
          </div>

          <div className="header--new-logo">
            <OptimusLogo width="7em" />
          </div>
        </div>

        <div className="read-more" onClick={scrollToMain}>
          <button className="read-more--arrow"></button>
        </div>
      </header>

      <main ref={mainRef} className="main-content">
        {pageText.text1[lang]}
        
        <div className="media-container">
          <ImageLoader src={oldLogo} alt="Older LinuxOptimus (a.k.a. 'Как приручить трансформера') logo" width="20rem" />
        </div>

        {pageText.text2[lang]}

        <div className="media-container">
          <ImageLoader src={inkscapePhoto} alt="Drawing new logo using Inkscape" width="50rem" aspect={560/420} />
        </div>
          
        {pageText.text3[lang]}

        <div className="media-container">
          <OptimusLogo width="25rem" fill="#204060" />
        </div>

        {pageText.text4[lang]}

        <div className="media-container">
          <div style={{backgroundColor : 'black', padding: '2rem'}}>
            <OptimusLogo width="20rem" fill="#f1f2f3" />
          </div>
        </div>

        {pageText.text5[lang]}

        <div className="media-container" children={
            !videoFailed
            ? (
              <video preload="metadata" controls playsInline loop muted poster={logoTransitionPoster} onPlay={() => { window.gtag('event', 'play_video', {'video' : 'logo-transition.mp4'}) }}>
                <source src="./video/logo-transition.mp4" type="video/mp4" />
              </video>
              ) 
            : <YouTubeEmbed />
          }>
        </div>

        <div style={{
          textAlign: 'center',
          marginTop: '1rem',
          fontSize: '.75em'
        }}>
          <a href="https://www.youtube.com/Xpt7melZbR0" onClick={videoFallback}>
            {!videoFailed && <span>({pageText.showYouTubeLink[lang]})</span> }
            {videoFailed && <span>({pageText.showVideoLink[lang]})</span> }
          </a>
        </div>

        {pageText.text6[lang]}

        <LogoVariations />

        {pageText.text7[lang]}

        <div className="media-container">
          <ImageLoader src={svgCodePhoto} alt="SVG code created by Inkscape" width="50rem" aspect={693/627} />
        </div>

        {pageText.text8[lang]}

        <div className="media-container">
          <ImageLoader src={logoReactComponentCode} alt="SVG logo converted into a ReactJS component" width="50rem" aspect={809/782} />
        </div>

        {pageText.text9[lang]}

        <div className="media-container">
          <ImageLoader src={logoReactOptionsCode} alt="options to set props for React logo component" width="50rem" aspect={650/530} />
        </div>

        {pageText.text10[lang]}

        <PlayLogoOptions lang={lang} />
        
        {pageText.text11[lang]}

        <PlayLogoAnimation lang={lang} />

        {pageText.text12[lang]}

        <ChannelsLinks />

        {pageText.text13[lang]}

        <p>
          <a href="./download/LinuxOptimus.svg" className="button" download onClick={() => { window.gtag('event', 'svg_logo_download'); }}>
            <OptimusLogo width="1.5em" className="glowing-eyes" /> 
              <span>&nbsp;{pageText.downloadLink[lang]}&nbsp;</span>
            <OptimusLogo width="1.5em" className="glowing-eyes" /> 
          </a>
        </p>

      </main>
    
      <Footer lang={lang}>
        <LangSelect lang={lang} onSelectLang={selectLang} where="footer" />
      </Footer>
    </>
  );
}

export default App;
