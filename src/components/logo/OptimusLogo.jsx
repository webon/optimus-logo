import React from 'react'

/**
 * component usage examples:
 * <OptimusLogo width="10rem" />
 * <OptimusLogo blind={true} stroke="black" fill="red" strokeWidth="4" />
 * <OptimusLogo eyesStroke="#000000" eyesFill="#ff0000" eyesStrokeWidth="4" />
 * 
 * @param {ViewBox} props.viewBox         ViewBox attr for svg. Defaults to '0 0 256 256'.
 * @param {Measure} props.width           Width for the svg style. Defaults to 100%.
 * @param {Measure} props.height          Height for the svg style. Defaults to auto.
 * @param {Boolean} props.blind           Removes eyes from the logo.
 * @param {Color}   props.stroke          Stroke color. Defaults to 'none'.
 * @param {Number}  props.strokeWidth     Stroke width.
 * @param {Color}   props.fill            Fill color. Defaults to current color.
 * @param {Color}   props.eyesStroke      Stroke color for the eyes. Defaults to main stroke color.
 * @param {Number}  props.eyesStrokeWidth Stroke width for the eyes. Defaults to main stroke width.
 * @param {Color}   props.eyesFill        Fill color for the eyes. Defaults to main fill color.
 */

export default function OptimusLogo(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox={props.viewBox || '0 0 256 256'} 
      style={{
        width: props.width || '100%',
        height: props.height || 'auto',
        ...props.style
      }}
      className={props.className}
    >
        
      <g stroke={props.stroke || 'none'} fill={props.fill || 'currentColor'} strokeWidth={props.strokeWidth}>

        <g className="optimus-logo--masks">
          <path d="m 73.756368,164.48859 51.531452,22.45453 -3e-5,64.55683 -51.531422,-25.26136 z" className="optimus-logo--mask optimus-logo--left" />

          <path d="m 182.24364,164.48859 -51.53146,22.45453 v 64.55683 l 51.53146,-25.26136 z" className="optimus-logo--mask optimus-logo--right" />
        </g>

        <path d="M 114.71721,175.91667 128,181.32951 l 13.56092,-5.61366 -8.13654,-30.87499 h -10.84879 z" className="optimus-logo--nose optimus-logo--center" />
        
        <g className="optimus-logo--foreheads">
          <path d="M 54.969397,63.309107 62.907649,116.7727 117.15123,133.6136 103.0479,113.96586 114.43903,49.409049 85.380221,44.028983 79.180716,69.056781 Z" className="optimus-logo--forehead optimus-logo--left" />

          <path d="m 116.06635,91.511337 -4.33947,22.454523 16.27345,22.45455 16.27277,-22.45455 -4.33948,-22.454523 z" className="optimus-logo--forehead optimus-logo--center" />

          <path d="m 201.03062,63.309107 -7.93825,53.463593 -54.24361,16.8409 14.10335,-19.64774 -11.39117,-64.556793 29.05883,-5.380073 6.19949,25.027787 z" className="optimus-logo--forehead optimus-logo--right" />
        </g>

        <path d="m 117.15123,85.897696 h 21.6975 l -5.42436,-44.909089 h -10.84878 z" className="optimus-logo--hair optimus-logo--center" />
        

        <g className="optimus-logo--cheeks">
          <path d="m 54.771115,125.19314 13.560892,36.48862 v 58.94318 l -40.68268,-19.64772 v -64.55681 z" className="optimus-logo--cheek optimus-logo--left" />

          <path d="m 201.22886,125.19314 -13.56087,36.48862 v 58.94318 l 40.68268,-19.64772 v -64.55681 z" className="optimus-logo--cheek optimus-logo--right" />
        </g>

        <g className="optimus-logo--ears">
          <path d="M 54.771115,115.0886 30.361503,125.19316 V 4.499955 l 13.638033,31.839478 v 0 0 z" className="optimus-logo--ear optimus-logo--left" />

          <path d="m 201.22889,115.0886 24.40961,10.10456 -2e-5,-120.6931938 -13.63801,31.8394768 v 0 0 z" className="optimus-logo--ear optimus-logo--right" />
        </g>


        {!props.blind && (
          <g stroke={props.eyesStroke} fill={props.eyesFill} strokeWidth={props.eyesStrokeWidth} className="optimus-logo--eyes">
            <path d="m 114.43903,142.03403 -51.531381,-16.84089 10.848719,28.06818 35.258322,8.42044 z" className="optimus-logo--eye optimus-logo--left" />
    
            <path d="m 141.56094,142.03403 51.53139,-16.84089 -10.84869,28.06818 -35.25836,8.42044 z" className="optimus-logo--eye optimus-logo--right" /> 
          </g>
        )}

      </g>
    </svg>
  )
}
