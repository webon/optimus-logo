import React from 'react'
import './LangSelect.css';

export default function LangSelect(props) {
  return (
    <div className="lang-select">
      <a rel="en" href="#/en" className={props.lang === 'en' ? 'active' : ''} onClick={props.onSelectLang} title="English" data-where={props.where}>EN</a>
      <a rel="ru" href="#/ru" className={props.lang === 'ru' ? 'active' : ''} onClick={props.onSelectLang} title="Русский" data-where={props.where}>RU</a>
    </div>
  )
}
