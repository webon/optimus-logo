import React from 'react'

import LazyLoad from 'react-lazy-load';

import './Footer.css';
import OptimusLogo from './logo/OptimusLogo';

import { footerText } from '../i18n/footerText';

export default function Footer(props) {
  return (
    <footer itemScope itemType="https://schema.org/WebSite">
      <div className="main-content">
        <div className="footer-grid">
          <div>
            <OptimusLogo width="5rem" />
          </div>

          <div className="grid-column-span-2">
            <LazyLoad>
              <a rel="license noreferrer" href="https://creativecommons.org/licenses/by-nc-sa/4.0/" onClick={() => { window.gtag('event', 'click_licence_link', { 'element' : 'logo'}); }} target="_blank">
                <img alt="Creative Commons License" style={{borderWidth : 0}} src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
              </a> 
            </LazyLoad>

            <div>
              <span {...{"xmlns:dct" : "https://purl.org/dc/terms/"}} href="https://purl.org/dc/dcmitype/StillImage" property="dct:title" rel="dct:type" itemProp="title">{footerText.logo_title[props.lang]}</span> 
              
              &nbsp;
              {footerText.by[props.lang]}
              &nbsp;
              
              <a {...{"xmlns:cc" : "http://creativecommons.org/ns#"}} href="https://www.linkedin.com/in/oleg-kiselevich-9744143b/" property="cc:attributionName" rel="cc:attributionURL noreferrer" itemProp="author" onClick={() => { window.gtag('event', 'click_linkedin_link', {'for' : 'Oleg'}); }} target="_blank">{footerText.author[props.lang]}</a> 
              
              &nbsp;
              {footerText.text1[props.lang]}
            </div>

            <div>
              <a rel="license noreferrer" href="https://creativecommons.org/licenses/by-nc-sa/4.0/" onClick={() => { window.gtag('event', 'click_licence_link', { 'element' : 'link'}); }} target="_blank">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
            </div>
          </div>

          <div>
            {footerText.text2[props.lang]}
            &nbsp;
            <a href="https://gitlab.com/webon/optimus-logo" onClick={() => { window.gtag('event', 'click_source_code_link'); }} target="_blank" rel="noreferrer">GitLab WebON repository</a>
          </div>
        </div>

        <div style={{
          display: 'flex', 
          justifyContent: 'space-between', 
          alignItems: 'baseline'
        }}>
          <p>
            <small>{footerText.text3[props.lang]}: <b itemProp="datePublished" content="2020-11-04">{footerText.date[props.lang]}</b></small>
          </p>
          
          {props.children}
        </div>

      </div>
    </footer>
  )
}
