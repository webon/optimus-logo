import React from 'react';
import OptimusLogo from './logo/OptimusLogo';

export default function LogoVariations() {
  return (
    <div className="media-container-grid">
      <div>
        <OptimusLogo width="15rem" fill="teal" blind />
      </div>
      <div style={{backgroundColor: 'slategrey'}}>
        <OptimusLogo width="15rem" fill="#f3f2f1" blind />
      </div>
      <div>
        <OptimusLogo width="15rem" fill="none" stroke="#305040" strokeWidth="4" />
      </div>
      <div style={{backgroundColor: '#202030'}}>
        <OptimusLogo width="15rem" fill="none" stroke="#f3f2f1" strokeWidth="3" />
      </div>

      <div>
        <OptimusLogo width="15rem" fill="none" stroke="indigo" strokeWidth="4" blind />
      </div>

      <div style={{backgroundColor: 'darkslategrey'}}>
        <OptimusLogo width="15rem" fill="none" stroke="snow" eyesFill="snow" eyesStroke="none" strokeWidth="4" />
      </div>

      <div>
        <OptimusLogo width="15rem" fill="none" stroke="slategrey" eyesFill="firebrick" eyesStroke="none" strokeWidth="4" />
      </div>

      <div style={{backgroundColor: 'darkred'}}>
        <OptimusLogo width="15rem" fill="ivory" stroke="none" eyesFill="none" eyesStroke="ivory" eyesStrokeWidth="4" />
      </div>
    </div>
  )
}
