import React from 'react';
import OptimusLogo from './logo/OptimusLogo';

import { useState } from 'react';

import './PlayLogoOptions.css';

export default function PlayLogoOptions(props) {
  const [Blind, setBlind] = useState(false);
  const [Bg, setBg] = useState('#ffffff');
  const [Fill, setFill] = useState('#008b8b');
  const [Stroke, setStroke] = useState('#ffffff');
  const [StrokeWidth, setStrokeWidth] = useState(0);
  const [EyesFill, setEyesFill] = useState('#8b0000');
  const [EyesStroke, setEyesStroke] = useState('#ffffff');
  const [EyesStrokeWidth, setEyesStrokeWidth] = useState(0);

  const labels = [
    (props.lang === 'en' ? 'Eyes variation' : 'Наличие глаз'),
    (props.lang === 'en' ? 'Blind' : 'Ослепить'),
    (props.lang === 'en' ? 'Unblind' : 'Прозреть'),
    (props.lang === 'en' ? 'Background' : 'Цвет фона'),
    (props.lang === 'en' ? 'Fill' : 'Заливка'),
    (props.lang === 'en' ? 'Stroke' : 'Контур'),
    (props.lang === 'en' ? 'Stroke width' : 'Обводка'),
    (props.lang === 'en' ? 'Eyes fill' : 'Заливка глаз'),
    (props.lang === 'en' ? 'Eyes stroke' : 'Контур глаз'),
    (props.lang === 'en' ? 'Eyes stroke width' : 'Обводка глаз')
  ];

  return (
    <div className="media-container">
      <div className="controls-container">
        <label>
          <span>{labels[0]}</span>
          <button onClick={e => { setBlind(!Blind); window.gtag('event', 'play_logo_options', { 'option' : 'blind', 'value': !Blind}) }}><span>{Blind ? labels[2] : labels[1]}</span></button>
        </label>
        
        <label>
          <span>{labels[3]}</span>
          <input type="color" defaultValue={Bg} onChange={e => { setBg(e.target.value); window.gtag('event', 'play_logo_options', { 'option' : 'background', 'value': e.target.value}) }} />
        </label>

        <label>
          <span>{labels[4]}</span>
          <input type="color" defaultValue={Fill} onChange={e => { setFill(e.target.value); window.gtag('event', 'play_logo_options', { 'option' : 'fill', 'value': e.target.value}) }} />
        </label>

        <label>
          <span>{labels[5]}</span>
          <input type="color" defaultValue={Stroke} onChange={e => { setStroke(e.target.value); window.gtag('event', 'play_logo_options', { 'option' : 'stroke', 'value': e.target.value}) }} />
        </label>

        <label>
          <span>{labels[6]}</span>
          <input type="range" value={StrokeWidth} min="0" max="10" onChange={e => { setStrokeWidth(e.target.value); window.gtag('event', 'play_logo_options', { 'option' : 'stroke_width', 'value': e.target.value}) }} />
        </label>

        <label>
          <span>{labels[7]}</span>
          <input type="color" defaultValue={EyesFill} onChange={e => { setEyesFill(e.target.value); window.gtag('event', 'play_logo_options', { 'option' : 'eyes_fill', 'value': e.target.value}) }} />
        </label>

        <label>
          <span>{labels[8]}</span>
          <input type="color" defaultValue={EyesStroke} onChange={e => { setEyesStroke(e.target.value); window.gtag('event', 'play_logo_options', { 'option' : 'eyes_stroke', 'value': e.target.value}) }} />
        </label>

        <label>
          <span>{labels[9]}</span>
          <input type="range" value={EyesStrokeWidth} min="0" max="10" onChange={e => { setEyesStrokeWidth(e.target.value); window.gtag('event', 'play_logo_options', { 'option' : 'eyes_stroke_width', 'value': e.target.value}) }} />
        </label>
      </div>

      <div style={{backgroundColor : Bg, maxWidth: '50%'}}>
        <OptimusLogo width="25rem" 
          blind={Blind}
          fill={Fill} 
          stroke={Stroke} 
          strokeWidth={StrokeWidth}
          eyesFill={EyesFill}
          eyesStroke={EyesStroke}
          eyesStrokeWidth={EyesStrokeWidth}
        />
      </div>
    </div>
  )
}
