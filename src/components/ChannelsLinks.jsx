import React from 'react';
import ImageLoader from './ImageLoader';

import ytLogo from '../assets/brand/yt_icon_rgb.png';
import igLogo from '../assets/brand/ig_logo.png';
import tLogo from '../assets/brand/t_logo.png';
import vkLogo from '../assets/brand/vk_logo.png';

import './ChannelsLinks.css';

export default function ChannelsLinks() {
  return (
    <div className="media-container socials-container">
      <a href="https://www.youtube.com/channel/UCxPdYLJOUjuw4_YX-L_Q16Q" onClick={() => { window.gtag('event', 'click_channel_link', {'channel' : 'YouTube'}); }} target="_blank" rel="noreferrer">
        <ImageLoader src={ytLogo} alt="YouTube" width="7rem" aspect={7/5} />
      </a>

      <a href="https://t.me/joinchat/AAAAAFlFK_b6rSdliVo_gA" onClick={() => { window.gtag('event', 'click_channel_link', {'channel' : 'Telegram'}); }} target="_blank" rel="noreferrer">
        <ImageLoader src={tLogo} alt="Telegram" width="7rem" aspect={7/5} />
      </a>

      <a href="https://vk.com/linuxoptimus" onClick={() => { window.gtag('event', 'click_channel_link', {'channel' : 'VK'}); }} target="_blank" rel="noreferrer">
        <ImageLoader src={vkLogo} alt="VK.com" width="7rem" aspect={7/5} />
      </a>

      <a href="https://www.instagram.com/linuxoptimus/" onClick={() => { window.gtag('event', 'click_channel_link', {'channel' : 'Instagram'}); }} target="_blank" rel="noreferrer">
        <ImageLoader src={igLogo} alt="Instagram" width="7rem" aspect={7/5} />
      </a>
    </div>
  )
}
