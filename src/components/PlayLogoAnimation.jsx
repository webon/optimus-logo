import React from 'react';
import OptimusLogo from './logo/OptimusLogo';

import { useState } from 'react';

import './PlayLogoAnimation.css';

export default function PlayLogoAnimation(props) {
  const [animated, setAnimated] = useState(true);
  const uniteText = (props.lang === 'en') ? 'Let\'s unite!' : 'Слияние!';
  const splitText = (props.lang === 'en') ? 'Let\'s split!' : 'Разъединить!';

  return (
    <div className="media-container">
      <div>
        <center>
          <button onClick={e => { setAnimated(!animated); window.gtag('event', 'play_logo_animation', {'animation' : (animated ? 'unite' : 'split')}); }}><span>{animated ? uniteText : splitText}</span></button>
        </center>

        <OptimusLogo width="30rem"
          viewBox="-128 -128 512 512" 
          fill="none" 
          stroke="slategrey" 
          strokeWidth="4"
          eyesFill="seagreen" 
          eyesStroke="none"
          eyesStrokeWidth="0"
          className={`animated-logo ${animated ? 'animate' : ''}`}
        />
      </div>
    </div>
  )
}
