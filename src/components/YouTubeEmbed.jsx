import React, { useState } from 'react'

import './YouTubeEmbed.css';

export default function YouTubeEmbed(props) {
  const [frameLoaded, setFrameLoaded] = useState(false);

  return (
    <div style={{
      position: 'relative',
      width: '100%',
      height: '0',
      paddingBottom: 'calc(100% / 16 * 9)'
    }} className={frameLoaded ? 'iframe-loaded' : 'iframe-loading'}> 
      <iframe 
        width="480"
        height="270"
        title="LinuxOptimus Logo video transition example" 
        
        src="https://www.youtube.com/embed/Xpt7melZbR0?mute=1&amp;loop=1&amp;list=PLp5xSn7Y0KQQaB_-cllHEW85DD-q5nSTg&amp;playsinline=1&amp;origin=https://webon.gitlab.io/optimus-logo/" 
        
        frameBorder="0" 
        allow="encrypted-media; picture-in-picture" 
        allowFullScreen 

        onLoad={() => { setFrameLoaded(true) }}
        
        style={{
          position: 'absolute',
          left: '0',
          top: '0', 
          width: '100%', 
          height: '100%'
        }}></iframe>
    </div>
  )
}
